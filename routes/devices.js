const express = require('express');
const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi);
const devicesController = require('../controllers/devices.controller');
const { validateBody, validateParams } = require('../middlewares/route');

const router = express.Router();

// POST http://localhost:3000/devices/
router.post('/', validateBody(Joi.object().keys({
  name: Joi.string().required().description('Device name.'),
  os: Joi.string().required().description('Operating system of device.'),
  manufacturer: Joi.string().required().description('Manufacturer of device.'),
  lastCheckedOutDate: Joi.date().required().description('lastCheckedOut Date.'),
  lastCheckedOutBy: Joi.string().required().description('lastCheckedOut by whom.'),
  isCheckedOut: Joi.bool().default(false).description('isCheckedOut.')
}),
  {
    stripUnknown: true,
  }),
  devicesController.createDevice
);

// GET http://localhost:3000/devices/
router.get('/', devicesController.getAllDevices);

// GET http://localhost:3000/devices/:id
router.get('/:id',
  validateParams(Joi.object().keys({
    id: Joi.objectId().description('Device Id'),
  }),
    {
      stripUnknown: true,
    }),
  devicesController.getDevice
);

// DELETE http://localhost:3000/devices/:id
router.delete('/:id', devicesController.deleteDevice);

module.exports = router;