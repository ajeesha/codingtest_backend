const { MongoMemoryServer } = require('mongodb-memory-server');
const mongoose = require('mongoose');

const port = 27017;
const mongod = new MongoMemoryServer({
  instance: {
    port,
  },
  autoStart: false,
});

const setup = async () => {
  await mongod.start();
  if (mongod.instanceInfoSync.port !== port) {
    throw new Error(`Failed to startup, :${port} already in use`);
  }
  const connection = await mongoose.connect(`mongodb://localhost/deviceDatabase`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  console.log(connection);
};

before(async () => {
  await setup();
});