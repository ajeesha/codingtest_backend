const request = require('supertest');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const app = require('../app');
const expect = chai.expect;
const should = chai.should;
const have = chai.have;
chai.use(chaiAsPromised);

describe('Test cases for - devices', () => {
  it('should fail to create a device without a name', async () => {
    const res = await request(app).post('/devices').send({
      "os": "Android 4.3",
      "manufacturer": "Motorola",
      "lastCheckedOutDate": "2016-01-21T09:10:00--05:00",
      "lastCheckedOutBy": "Ian",
      "isCheckedOut": true,
    });
    expect(res.status).to.equal(400);
    expect(res.body.message).to.equal(`Device name required.`);
  });
   // Test to create devices
  it('should create a devices', async () => {
    const device = {
      "name": "Moto G",
      "os": "Android 4.3",
      "manufacturer": "Motorola",
      "lastCheckedOutDate": "2016-01-21T09:10:00--05:00",
      "lastCheckedOutBy": "Ian",
      "isCheckedOut": true,
    };
    const res = await request(app).post('/devices').send(device);
    expect(res.status).to.equal(204);
    // check whether device got added
    const resp = await request(app).get('/devices');
    expect(resp.body.name).to.equal(device.name);
    expect(resp.body.os).to.equal(device.os);
    expect(resp.body.manufacturer).to.equal(device.manufacturer);
    expect(resp.body.lastCheckedOutDate).to.equal(device.lastCheckedOutDate);
    expect(resp.body.lastCheckedOutBy).to.equal(device.lastCheckedOutBy);
    expect(resp.body.isCheckedOut).to.equal(device.isCheckedOut);
  });
  // Test to get all devices
  it('Should get all devices record', (done) => {
        request(app)
        .get('/devices')
        .end(function (err, res) {
            const result = res.statusCode
            expect(result).to.equal(200)
            done()
        })
    })
     it('should fail to get device record with null id', (done) => {
        const id = null;
        request(app).get(`/devices/${id}`)
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        });

      });
    // Test to get single device record
    it("should get device by id", (done) => {
        const id = "6000785c054e852bf85c892d";
        request(app).get(`/devices/${id}`)
        .end((err, res) => {
            expect(res.status).to.equal(200);
            expect(res.body).to.be.a('object');
            done();
        });
   });
   // Test to delete device record
   it("should delete device by id", (done) => {
        const id = "6000785c054e852bf85c892d";
        request(app).delete(`/devices/${id}`)
        .end((err, res) => {
            expect(res.status).to.equal(200);
            expect(res.body).to.be.a('object');
            done();
        });
    });
});



