const mongoose = require('mongoose');

const DevicesSchema = new mongoose.Schema ({
  name: mongoose.Schema.Types.String,
  os: mongoose.Schema.Types.String,
  manufacturer: mongoose.Schema.Types.String,
  lastCheckedOutDate: mongoose.Schema.Types.Date,
  lastCheckedOutBy: mongoose.Schema.Types.String,
  isCheckedOut: mongoose.Schema.Types.Boolean,
});

module.exports = mongoose.model('Devices', DevicesSchema);