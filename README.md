## Getting Started:
Built for Node Version: 12.x

# Install  dependencies
npm install

This will run **node server** at localhost:3000. Once server started open **postman** and using **POST**, **GET**, **Delete** method you can access the URL http://localhost:3000/devices

# POST Request Example
{
    "name": "Moto G",
    "os": "Android 4.3",
    "manufacturer": "Motorola",
    "lastCheckedOutDate": "2016-01-21T09:10:00--05:00",
    "lastCheckedOutBy": "Ian",
    "isCheckedOut": true,
}






