
const Devices = require('../models/devices');

const devicesController = {
   async createDevice(req, res, next) {
      const device = new Devices(req.body);
      await device.save();
      res.status(204);
   },
   async getAllDevice(req, res, next) {
      const devices = await Devices.find();
      res.status(200).json(devices);
   },
   async getDevice(req, res, next) {
      const deviceId = req.params.id;
      const device = await Devices.findOne({ _id: deviceId });
      res.status(200).json(device);
   },
   async deleteDevice(req, res, next) {
      const deviceId = req.params.id;
      const devices = await Devices.deleteOne({ _id: deviceId });
      res.status(204).json(devices);
   },
};

module.exports = devicesController;
